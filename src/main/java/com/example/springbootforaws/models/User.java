package com.example.springbootforaws.models;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data

public class User {
    private Long id;
    private String firstName;
    private String lastName;
    private int age;

    private static Long idCounter = 1L;

    public User() {
        this.id = idCounter++;
    }

    public User(String firstName, String lastName, int age) {
        this.id = idCounter++;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
}
