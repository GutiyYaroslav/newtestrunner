FROM openjdk:17-alpine

COPY target/myapp.jar myapp.jar

ENTRYPOINT ["java", "-jar", "myapp.jar"]